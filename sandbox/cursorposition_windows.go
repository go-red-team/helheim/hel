//go:build windows
// +build windows

package sandbox

import (
	"time"
	"unsafe"
)

// CursorPosition returns true if the cursor position remains the same for the specified number of seconds, otherwise false.
func CursorPosition(seconds int) bool {

	// Assign point to the POINT struct
	point := POINT{}

	// Get the cursor position
	getCursorPos.Call(uintptr(unsafe.Pointer(&point)))

	// Sleep for the specified number of seconds
	time.Sleep(time.Duration(seconds) * time.Second)

	// Assign point2 to the POINT struct
	point2 := POINT{}

	// Get the cursor position again
	getCursorPos.Call(uintptr(unsafe.Pointer(&point2)))

	if point.X-point2.X == 0 && point.Y-point2.Y == 0 {
		return true
	}

	return false
}
