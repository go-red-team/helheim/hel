package sandbox

import (
	"net"
	"testing"
)

func TestCheckMACAddress(t *testing.T) {
	// Get the list of network interfaces
	interfaces, err := net.Interfaces()
	if err != nil {
		t.Fatal(err)
	}

	// Make sure there is at least one interface
	if len(interfaces) == 0 {
		t.Fatal("No network interfaces found")
	}

	// Use the MAC address of the first interface for testing
	macAddress := interfaces[0].HardwareAddr.String()

	// Test case 1: MAC address exists in the list
	macAddresses := []string{macAddress}
	result := CheckMACAddress(macAddresses)
	if !result {
		t.Error("CheckMACAddress returned false; want true")
	}

	// Test case 2: MAC address does not exist in the list
	macAddresses = []string{"11:22:33:44:55:66", "AA:BB:CC:DD:EE:FF"}
	result = CheckMACAddress(macAddresses)
	if result {
		t.Error("CheckMACAddress returned true; want false")
	}
}
