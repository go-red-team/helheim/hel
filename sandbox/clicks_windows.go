//go:build windows
// +build windows

package sandbox

// CountClicks counts the number of mouse clicks before returning a true once it's been reached.
func CountClicks(minClicks int) bool {
	// Create a variable to store the count of mouse clicks
	count := 0

	// Loop until the count of mouse clicks is greater than or equal to the minimum number of clicks
	for count < minClicks {
		leftClick, _, _ := getAsyncKeyState.Call(uintptr(0x1))
		rightClick, _, _ := getAsyncKeyState.Call(uintptr(0x2))

		// Check if the left mouse button is pressed
		if leftClick%2 == 1 {
			count++
		}

		// Check if the right mouse button is pressed
		if rightClick%2 == 1 {
			count++
		}
	}

	return true

}
