package sandbox

import (
	"math"

	"github.com/pbnjay/memory"
)

// CheckRAM checks to see if the host has at least the specified amount of ram and if so it returns true.
func CheckRAM(minRAM int64) bool {
	// Get the total amount of RAM in bytes
	totalRAM := memory.TotalMemory()

	// Convert the total amount of RAM from bytes to gigabytes and round to the nearest whole number
	totalRAMGB := int64(math.Round(float64(totalRAM) / (1024 * 1024 * 1024)))

	// Check if the total amount of RAM is greater than or equal to the minimum amount of RAM
	return totalRAMGB >= minRAM
}
