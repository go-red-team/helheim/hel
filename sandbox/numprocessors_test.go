package sandbox

import (
	"testing"
)

func TestProcessorCheck(t *testing.T) {
	// Test case 1: Minimum number of processors is met
	minNumber := 2
	expected := true
	result := ProcessorCheck(minNumber)
	if result != expected {
		t.Errorf("ProcessorCheck(%d) = %v; want %v", minNumber, result, expected)
	}

	// Test case 2: Minimum number of processors is not met
	minNumber = 100
	expected = false
	result = ProcessorCheck(minNumber)
	if result != expected {
		t.Errorf("ProcessorCheck(%d) = %v; want %v", minNumber, result, expected)
	}
}
