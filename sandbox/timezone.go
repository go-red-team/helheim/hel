package sandbox

import "time"

// IsTimezoneUTC returns true if the current timezone is UTC, otherwise false.
func IsTimezoneUTC() bool {
	// Get the current timezone
	_, offsetFromUTC := time.Now().Zone()

	return offsetFromUTC == 0
}
