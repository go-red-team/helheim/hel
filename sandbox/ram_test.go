package sandbox

import (
	"testing"
)

func TestCheckRAM(t *testing.T) {
	// Test case 1: RAM is more than the available memory
	result := CheckRAM(300)
	if result {
		t.Error("CheckRAM returned true; want false")
	}

	// Test case 2: RAM is equal to the available memory
	result = CheckRAM(128)
	if !result {
		t.Error("CheckRAM returned false; want true")
	}

	// Test case 3: RAM is less than the available memory
	result = CheckRAM(14)
	if !result {
		t.Error("CheckRAM returned false; want true")
	}
}
