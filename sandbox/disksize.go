package sandbox

import (
	"github.com/shirou/gopsutil/v3/disk"
)

// CheckDiskSize checks the disk size of the current drive and returns true if it is equal to or greater than the provided size.
func CheckDiskSize(minSizeGB uint64) bool {
	// Get the disk usage of the root path
	usageStat, err := disk.Usage("/")
	if err != nil {
		return false
	}

	// Convert the total disk size from bytes to gigabytes
	diskSizeGB := usageStat.Total / (1024 * 1024 * 1024)

	// Check if the disk size is greater than or equal to the provided size
	return diskSizeGB >= minSizeGB
}
