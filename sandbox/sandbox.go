//go:build windows
// +build windows

package sandbox

import "golang.org/x/sys/windows"

var (
	kernel32         = windows.NewLazyDLL("kernel32.dll")
	user32           = windows.NewLazyDLL("user32.dll")
	getAsyncKeyState = user32.NewProc("GetAsyncKeyState")
	getCursorPos     = user32.NewProc("GetCursorPos")
)

type POINT struct {
	X, Y int32
}
