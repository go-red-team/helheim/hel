package sandbox

import (
	"os"
	"path/filepath"
)

// CurrentFilename returns true if the current filename matches the expected filename, otherwise false.
func CurrentFilename(expectedFilename string) bool {
	actualName := filepath.Base(os.Args[0])

	return actualName == expectedFilename
}
