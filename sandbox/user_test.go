package sandbox

import (
	"os/user"
	"testing"
)

func TestCheckUsername(t *testing.T) {
	// Get the current username
	currentUser, err := user.Current()
	if err != nil {
		t.Fatalf("Failed to get current user: %v", err)
	}

	// Test with the current username in the list of bad usernames
	if !CheckUsername([]string{currentUser.Username}) {
		t.Errorf("CheckUsername did not return true when the current username was in the list of bad usernames")
	}

	// Test with the current username not in the list of bad usernames
	if CheckUsername([]string{"badusername"}) {
		t.Errorf("CheckUsername did not return false when the current username was not in the list of bad usernames")
	}
}
