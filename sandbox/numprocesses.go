package sandbox

import (
	"github.com/shirou/gopsutil/v3/process"
)

// MinRunningProcs ensures that the number of running processes is more than the provided number.
func MinRunningProcs(numProcs int) bool {
	// Get a slice of the PIDs of all running processes
	pids, err := process.Pids()
	if err != nil {
		return false
	}

	// Check if the number of running processes is less than the provided number.
	return len(pids) >= numProcs
}
