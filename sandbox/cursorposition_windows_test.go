//go:build windows
// +build windows

package sandbox

import (
	"testing"
)

func TestCursorPosition(t *testing.T) {
	// Test case 1: Cursor position does not change after waiting for 2 seconds
	if result := CursorPosition(2); !result {
		t.Errorf("CursorPosition(2) = %v; want true", result)
	}

	// Test case 2: Cursor position changes after waiting for 5 seconds
	if result := CursorPosition(5); result {
		t.Errorf("CursorPosition(5) = %v; want false", result)
	}
}
