//go:build windows
// +build windows

package sandbox

import (
	"syscall"

	"golang.org/x/sys/windows"
)

func DetectDebugging() bool {
	kernel32, _ := windows.LoadLibrary("kernel32.dll")
	IsDebuggerPresent, _ := windows.GetProcAddress(kernel32, "IsDebuggerPresent")

	var nargs uintptr = 0

	if debuggerPresent, _, err := syscall.SyscallN(uintptr(IsDebuggerPresent), nargs, 0, 0, 0); err != 0 {
		return true
	} else {
		if debuggerPresent != 0 {
			return true
		}
	}

	return false
}
