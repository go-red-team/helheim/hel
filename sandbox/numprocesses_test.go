package sandbox

import (
	"testing"
)

func TestMinRunningProcs(t *testing.T) {
	// Test case 1: when the number of running processes is less than the provided number
	numProcs := 5000
	result := MinRunningProcs(numProcs)
	if result {
		t.Errorf("MinRunningProcs(%d) = %v; want false", numProcs, result)
	}

	// Test case 2: when the number of running processes is greater than the provided number
	numProcs = 15
	result = MinRunningProcs(numProcs)
	if !result {
		t.Errorf("MinRunningProcs(%d) = %v; want true", numProcs, result)
	}
}
