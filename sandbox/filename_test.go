package sandbox

import (
	"os"
	"path/filepath"
	"testing"
)

func TestCurrentFilename(t *testing.T) {
	// Get the path of the test binary
	executablePath, err := os.Executable()
	if err != nil {
		t.Fatalf("Failed to get executable path: %v", err)
	}

	// Extract the filename from the path
	expectedFilename := filepath.Base(executablePath)

	// Call the CurrentFilename function
	result := CurrentFilename(expectedFilename)

	// Check if the function returned true
	if !result {
		t.Errorf("CurrentFilename() = %v; want true", result)
	}

	// Set the command line arguments to include a different filename
	os.Args = []string{"", "otherfile.go"}

	// Call the CurrentFilename function again
	result = CurrentFilename(expectedFilename)

	// Check if the function returned false
	if result {
		t.Errorf("CurrentFilename() = %v; want false", result)
	}
}
