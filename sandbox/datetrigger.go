package sandbox

import "time"

// DateTrigger returns true if the current date is equal to the specified date, otherwise false.
func DateTrigger(date string) bool {
	// Parse the specified date
	triggerDateRaw, _ := time.Parse("01-02-2006", date)

	// Get the year, month, and day of the specified date
	triggerYear, triggerMonth, triggerDay := triggerDateRaw.Date()

	triggerDate := time.Date(triggerYear, triggerMonth, triggerDay, 0, 0, 0, 0, time.Now().Location())

	// Get the current date
	year, month, day := time.Now().Date()

	// Create a variable to store the current date
	today := time.Date(year, month, day, 0, 0, 0, 0, time.Now().Location())

	// Loop until the current date is equal to the specified date
	for today.Before(triggerDate) {
		// Sleep for 1 day
		time.Sleep(time.Duration(86340 * time.Second))
		// Get the current date
		year, month, day = time.Now().Date()
		// Store the current date
		today = time.Date(year, month, day, 0, 0, 0, 0, time.Now().Location())
	}

	return true

}
