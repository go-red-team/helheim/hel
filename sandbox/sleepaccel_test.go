package sandbox

import (
	"testing"
)

func TestGetSleepTime(t *testing.T) {
	// Test case 1: Sleep time is not accelerated
	sleepSeconds := 1
	accelerated := GetSleepTime(sleepSeconds)
	if accelerated {
		t.Errorf("GetSleepTime(%d) = %v; want false", sleepSeconds, accelerated)
	}

}
