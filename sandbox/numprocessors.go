package sandbox

import "runtime"

// ProcessorCheck checks the number of processor cores available on the system on returns true if the minimum number exists.
func ProcessorCheck(minNumber int) bool {
	// Get the number of processor cores
	cores := runtime.NumCPU()

	return cores >= minNumber
}
