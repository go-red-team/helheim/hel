package sandbox

import (
	"testing"
	"time"
)

func TestDateTrigger(t *testing.T) {
	// Get the current date
	year, month, day := time.Now().Date()

	// Format the current date as a string
	date := time.Date(year, month, day, 0, 0, 0, 0, time.Now().Location()).Format("01-02-2006")

	// Call the DateTrigger function with the current date
	result := DateTrigger(date)

	// Check if the function returned true
	if !result {
		t.Errorf("DateTrigger(%s) = %v; want true", date, result)
	}
}
