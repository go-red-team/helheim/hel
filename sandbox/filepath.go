package sandbox

import "os"

// CheckFilepaths checks for the existence of specified filepaths to determine if the program is running in a sandbox.
func CheckFilepaths(filepaths []string) bool {
	// Create a map of strings to store the evidence of a sandbox.
	EvidenceOfSandbox := make([]string, 0)

	// Loop through the filepaths and check if the file exists.
	for _, filepath := range filepaths {
		// If the file exists, add it to the evidence of a sandbox.
		if _, err := os.Stat(filepath); err == nil {
			EvidenceOfSandbox = append(EvidenceOfSandbox, filepath)
		}
	}
	return len(EvidenceOfSandbox) > 0
}
