package sandbox

import (
	"testing"
)

func TestCheckProcessNames_SandboxProcessExists(t *testing.T) {
	// Create a slice of process names
	processes := []string{
		"explorer",
	}

	// Call the CheckProcessNames function
	result := CheckProcessNames(processes)

	// Check if the result is true
	if !result {
		t.Errorf("CheckProcessNames() = %v; want true", result)
	}
}

func TestCheckProcessNames_NoSandboxProcessExists(t *testing.T) {
	// Create a slice of process names
	processes := []string{
		"nonexistentprocess",
	}

	// Call the CheckProcessNames function
	result := CheckProcessNames(processes)

	// Check if the result is false
	if result {
		t.Errorf("CheckProcessNames() = %v; want false", result)
	}
}
