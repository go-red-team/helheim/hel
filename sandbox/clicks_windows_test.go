//go:build windows
// +build windows

package sandbox

import (
	"testing"
)

func TestCountClicks(t *testing.T) {
	// Test case 1: minimum clicks is 0
	if !CountClicks(0) {
		t.Error("CountClicks(0) = false; want true")
	}

	// Test case 2: minimum clicks is 1
	if !CountClicks(1) {
		t.Error("CountClicks(1) = false; want true")
	}

	// Test case 3: minimum clicks is 5
	if !CountClicks(5) {
		t.Error("CountClicks(5) = false; want true")
	}
}
