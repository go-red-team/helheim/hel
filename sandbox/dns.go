package sandbox

import "net/http"

// CheckDNS checks if the DNS has been modified by the sandbox
func CheckDNS() bool {
	// Non-existent domain
	domain := "thisdomaindoesnotexist.com"

	// Create a http request to the domain
	response, err := http.Get("http://" + domain)

	if err != nil {
		return true
	}

	return response.StatusCode == 200
}
