package sandbox

import (
	"testing"
)

func TestCheckDiskSize(t *testing.T) {
	// Test case 1: Disk size is smaller than 2TB
	result := CheckDiskSize(2000)
	if result {
		t.Error("CheckDiskSize returned true; want false")
	}

	// Test case 2: Disk size is smaller than 1TB
	result = CheckDiskSize(1000)
	if result {
		t.Error("CheckDiskSize returned true; want false")
	}

	// Test case 3: Disk size is equal to or greater than 400GB
	result = CheckDiskSize(400)
	if !result {
		t.Error("CheckDiskSize returned false; want true")
	}
}
