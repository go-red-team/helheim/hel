package sandbox

import (
	"os"
	"testing"
)

func TestCheckFilepaths(t *testing.T) {
	// Test case 1: Empty filepaths
	filepaths := []string{}
	expectedResult := false
	result := CheckFilepaths(filepaths)
	if result != expectedResult {
		t.Errorf("CheckFilepaths(%v) = %v; want %v", filepaths, result, expectedResult)
	}

	// Test case 2: Non-existent filepaths
	filepaths = []string{"file1.txt", "file2.txt", "file3.txt"}
	expectedResult = false
	result = CheckFilepaths(filepaths)
	if result != expectedResult {
		t.Errorf("CheckFilepaths(%v) = %v; want %v", filepaths, result, expectedResult)
	}

	// Test case 3: Existing filepaths
	filepaths = []string{"existing_file1.txt", "existing_file2.txt", "existing_file3.txt"}
	for _, filepath := range filepaths {
		// Create empty files for testing
		file, err := os.Create(filepath)
		if err != nil {
			t.Fatalf("Failed to create test file: %v", err)
		}
		file.Close()
	}
	expectedResult = true
	result = CheckFilepaths(filepaths)
	if result != expectedResult {
		t.Errorf("CheckFilepaths(%v) = %v; want %v", filepaths, result, expectedResult)
	}
	for _, filepath := range filepaths {
		err := os.Remove(filepath)
		if err != nil {
			t.Errorf("Failed to remove test file: %v", err)
		}
	}
}
