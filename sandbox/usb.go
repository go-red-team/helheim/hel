//go:build windows
// +build windows

package sandbox

import "golang.org/x/sys/windows/registry"

// CheckUSB Checks the minimum number of USB devices connected to the system
func CheckUSB(minNumber int) bool {
	// Open the registry key for USB devices
	regKey, err := registry.OpenKey(registry.LOCAL_MACHINE, `SYSTEM\ControlSet001\Enum\USBSTOR`, registry.QUERY_VALUE)
	if err != nil {
		return false
	}

	defer regKey.Close()
	keyInfo, err := regKey.Stat()
	if err == nil {
		if int(keyInfo.SubKeyCount) < minNumber {
			return true
		}
		return false
	}
	return false
}
