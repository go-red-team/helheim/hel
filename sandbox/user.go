package sandbox

import (
	"os/user"
)

// CheckUsername checks to see if the username provided matches the current user
func CheckUsername(badUsernames []string) bool {
	// Get the current user
	currentUser, err := user.Current()
	if err != nil {
		return true
	}

	// Check if the current user is in the list of bad usernames
	for _, badUsername := range badUsernames {
		if currentUser.Username == badUsername {
			return true
		}
	}
	return false
}
