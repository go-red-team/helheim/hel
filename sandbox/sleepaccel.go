package sandbox

import (
	"errors"
	"time"

	"github.com/beevik/ntp"
)

// GetNTPTime returns the current NTP time from an NTP server.
func GetNTPTime(server string) (time.Time, error) {
	// Get the current NTP time
	ntpTime, err := ntp.Time(server)
	if err != nil {
		return time.Time{}, errors.New("failed to get NTP time")
	}

	return ntpTime, nil
}

// GetSleepTime checks for sleep acceleration by comparing the current NTP time
func GetSleepTime(sleepSeconds int) bool {
	// Get the NTP time
	firstTime, err := GetNTPTime("us.pool.ntp.org")

	// If we are unable to get NTP time, assume that the time has been accelerated
	if err != nil {
		return true
	}

	// Sleep for the specified time
	time.Sleep(time.Duration(sleepSeconds) * time.Second)

	// Get the NTP time again
	secondTime, _ := GetNTPTime("us.pool.ntp.org")

	// Check if the time has been accelerated
	return secondTime.Sub(firstTime) < time.Duration(sleepSeconds)*time.Second
}
