package sandbox

import (
	"strings"

	"github.com/shirou/gopsutil/v3/process"
)

// CheckProcessNames returns true if the names of any processes contain the names of processes commonly found in sandboxes, otherwise false.
func CheckProcessNames(processNames []string) bool {
	// Make a slice of strings to store the evidence of a sandbox
	EvidenceOfSandbox := make([]string, 0)

	// Get a slice of the PIDs of all running processes
	pids, _ := process.Pids()

	// Iterate through all processes
	for _, pid := range pids {
		proc, err := process.NewProcess(pid)
		if err != nil {
			continue
		}

		// Get the name of the process
		name, err := proc.Name()
		if err != nil {
			continue
		}

		// Iterate through the names of the sandbox processes
		for _, processName := range processNames {
			// Check if the name of the process contains the name of a sandbox process
			if strings.Contains(strings.ToLower(name), strings.ToLower(processName)) {
				// Append the name of the process to the slice if it is a sandbox process
				EvidenceOfSandbox = append(EvidenceOfSandbox, name)
			}
		}
	}

	return len(EvidenceOfSandbox) > 0
}
