//go:build windows
// +build windows

package sandbox

import (
	"testing"
)

func TestDetectDebugging(t *testing.T) {
	// Test case 1: Debugger is not present
	if result := DetectDebugging(); result {
		t.Errorf("DetectDebugging() = %v; want false", result)
	}
}
