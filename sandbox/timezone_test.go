package sandbox

import (
	"testing"
	"time"
)

func TestIsTimezoneUTC(t *testing.T) {
	// Save the original timezone
	originalLocation := time.Local

	// Set the timezone to UTC
	time.Local = time.UTC

	// Call the IsTimezoneUTC function
	result := IsTimezoneUTC()

	// Check if the function returned true
	if !result {
		t.Errorf("IsTimezoneUTC() = %v; want true", result)
	}

	// Set the timezone to something other than UTC
	time.Local = time.FixedZone("Non-UTC", 1)

	// Call the IsTimezoneUTC function again
	result = IsTimezoneUTC()

	// Check if the function returned false
	if result {
		t.Errorf("IsTimezoneUTC() = %v; want false", result)
	}

	// Restore the original timezone
	time.Local = originalLocation
}
