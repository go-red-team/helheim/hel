package sandbox

import (
	"net"
	"strings"
)

// CheckMACAddress checks if the interface MAC address partially matches the given MAC addresses.
func CheckMACAddress(macAddresses []string) bool {
	// Create a slice of MAC addresses to store evidence of a sandbox
	EvidenceOfSandbox := make([]net.HardwareAddr, 0)

	// Get all network interfaces
	NICs, _ := net.Interfaces()

	for _, NIC := range NICs {
		// Iterate over the given MAC addresses
		for _, mac := range macAddresses {
			if strings.Contains(strings.ToLower(NIC.HardwareAddr.String()), strings.ToLower(mac)) {
				EvidenceOfSandbox = append(EvidenceOfSandbox, NIC.HardwareAddr)
			}
		}
	}
	return len(EvidenceOfSandbox) > 0
}
