module gitlab.com/go-red-team/helheim/hel

go 1.22.0

require (
	github.com/beevik/ntp v1.3.1
	github.com/ecies/go/v2 v2.0.9
	github.com/pbnjay/memory v0.0.0-20210728143218-7b4eea64cf58
	github.com/shirou/gopsutil/v3 v3.24.2
	github.com/stretchr/testify v1.8.4
	golang.org/x/crypto v0.21.0
	golang.org/x/sys v0.18.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.2.0 // indirect
	github.com/ethereum/go-ethereum v1.13.5 // indirect
	github.com/go-ole/go-ole v1.3.0 // indirect
	github.com/lufia/plan9stats v0.0.0-20230326075908-cb1d2100619a // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/power-devops/perfstat v0.0.0-20221212215047-62379fc7944b // indirect
	github.com/shoenig/go-m1cpu v0.1.6 // indirect
	github.com/tklauser/go-sysconf v0.3.12 // indirect
	github.com/tklauser/numcpus v0.6.1 // indirect
	github.com/yusufpapurcu/wmi v1.2.4 // indirect
	golang.org/x/net v0.21.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
