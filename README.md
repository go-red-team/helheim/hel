# Hel
A simple to use malware development library designed for research purposes, red teaming, and penetration testing. This package was just made to facilitate making agents for the C2 [Fenrir](https://gitlab.com/go-red-team/helheim/fenrir)

## Features
Hel provides a wide range of features organized into various packages:

- `antire`: Techniques to prevent reverse engineering of the malware.
- `crypto`: Cryptographic functions for secure communication and data protection.
- `escalate`: Techniques for escalating privileges in the system.
- `exec`: Functions for executing implant specific commands.
- `files`: Functions for manipulating the file system.
- `injection`: Techniques for injecting code into processes.
- `logging`: Utilities for logging activities and debugging.
- `misc`: Miscellaneous utilities that don't fit into other categories.
- `network`: Functions for network communication and manipulation.
- `persistence`: Techniques for maintaining persistence after system reboots.
- `process`: Functions for process manipulation.
- `recon`: Utilities for gathering information about the system and environment.
- `regex`: Functions for regular expression matching.
- `sandbox`: Functions for evading sandboxes.
- `scanning`: Utilities for scanning the system or network.
- `shell`: Functions for executing shell commands.
- `shellcode`: Utilities for working with shellcode.
- `system`: Functions for manipulating the system, with specific packages for Linux, Mac, and Windows.
- `test`: Code tests to ensure the functionality of the library.
- `x`: Experimental packages for future features.
  
<details>
<br>

### Anti-Reversing
<details>
<br>
</details>

### Crypto
<details>
<br>

- AES
- Base32
- Base64
- Bcrypt
- ChaCha20
- Elliptic Curve
- MD5
- RC4
- ROT13
- ROT47
- SHA1
- SHA256
- SHA512
- Triple DES
- XOR
</details>

### Environment
<details>
<br>

- Domain Info
- Environment Variables
- Groups
- Username
- Hardware Info
- OS Info

</details>

### Privilege Escalation
<details>
<br>
</details>

### Exec
<details>
<br>
</details>

### Files
<details>
<br>

- Create Files
- Delete Files
- List Files
- Timestomp Files
- Change File Permissions

</details>

### Injection
<details>
<br>
</details>

### Logging
<details>
<br>
</details>

### Misc
<details>
<br>
</details>

### Network
<details>
<br>
</details>

### Persistence
<details>
<br>
</details>

### Process
<details>
<br>
</details>

### Reconnaissance
<details>
<br>
</details>

### Regex
<details>
<br>
</details>

### Sandbox
<details>
<br>

- Timezone Check
- Check Process Names
- Click Counter (Windows)
- Cursor Position (Windows)
- Date trigger
- Debugging Check (Windows)
- Disk Size Check
- Filepath Check
- MAC Address Check
- RAM Check
- Minimum Running Processes
- Sleep Acceleration Check
- USB Count (Windows)
- Processor Core Count

</details>

### Scanning
<details>
<br>
</details>

### Shell
<details>
<br>

- Linux Command Execution
- CMD Command Execution (Windows)
- Powershell Command Execution (Windows)
- Powershell Command Execution w/ Token (Windows)

</details>

### Shellcode
<details>
<br>
</details>

### System
<details>
<br>
</details>

</details>

## Installation
To install Hel, you need to have Go installed on your machine. If you don't have go installed you can download it from the [official Go website](https://go.dev).

Once you have Go installed, you can install Hel in your project by running the following command in your terminal:

```bash
go get -u gitlab.com/go-red-team/helheim/hel
```

This command will download and install Hel and it's dependencies

## Usage
After installing Hel, you can import it in your Go programs like any other package:

```go
import "gitlab.com/go-red-team/helheim/hel/crypto"
```

Then, you can use the functions provided by Hel's packages. For example, to use the AES encryption function from the `crypto` package, you can do:

```go
encryptedData, err := crypto.AESEncyrpt(data, key)
if err != nil {
    log.Fatal(err)
}
```

Please refer to the individual package documentation for more details on how to use these features.

## Contributing
We welcome contributions to Hel! Please see our [Contribution Guidelines](CONTRIBUTING.md) for more details on how you can help.

### Reporting Issues
If you find a bug or want to suggest a new feature, please [create a new issue](https://gitlab.com/go-red-team/helheim/hel/issues/new) on our GitLab repository. When reporting a bug, please include as much information as possible, including steps to reproduce the bug, the expected behavior, and the actual behavior.

### Submitting Pull Requests
If you've fixed a bug or implemented a new feature, you can submit a pull request. Here's how:

1. Fork the repository
2. Create a new branch for your changes.
3. Make your changes in your branch.
4. Run the tests to ensure your changes don't break existing functionality.
5. Submit a pull request with a description of your changes.

We'll review your pull request as soon as we can. Thanks for your contribution!

### Code of Conduct
We expect all contributors to follow our [Code of Conduct](CODE_OF_CONDUCT.md). Please read it before contributing.

### Questions

If your have any questions about contributing, please [open a new issue](https://gitlab.com/go-red-team/helheim/hel/issues/new) and we'll be happy to help.

## License
Hel is released under the [GPL v3](LICENSE).

## Credits
This wouldn't have been possible without the following repos
- [https://github.com/D3Ext/maldev](https://github.com/D3Ext/maldev)
- [https://github.com/redcode-labs/Coldfire](https://github.com/redcode-labs/Coldfire)
- [https://github.com/Arvanaghi/CheckPlease](https://github.com/Arvanaghi/CheckPlease)

## Disclaimer

Hel is developed for educational, red teaming, and research purposes only. It should not be used for malicious activities. The developers assume no liability for misuse of this library.