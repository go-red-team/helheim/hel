package crypto

import (
	"reflect"
	"testing"
)

func TestXOR(t *testing.T) {
	// Test case 1
	buf := []byte{0x01, 0x02, 0x03, 0x04}
	xor := []byte{0xAA, 0xBB, 0xCC}
	expected := []byte{0xAB, 0xB9, 0xCF, 0xAE} // Corrected expected result
	result := XOR(buf, xor)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("XOR(%v, %v) = %v; want %v", buf, xor, result, expected)
	}

	// Test case 2
	buf = []byte{0x11, 0x22, 0x33, 0x44, 0x55}
	xor = []byte{0xFF, 0x00}
	expected = []byte{0xEE, 0x22, 0xCC, 0x44, 0xAA} // Corrected expected result
	result = XOR(buf, xor)
	if !reflect.DeepEqual(result, expected) {
		t.Errorf("XOR(%v, %v) = %v; want %v", buf, xor, result, expected)
	}

	// Add more test cases here...
}
