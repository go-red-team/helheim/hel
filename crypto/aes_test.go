package crypto

import (
	"reflect"
	"testing"
)

func TestAESEncrypt(t *testing.T) {
	plaintext := []byte("Hello, World!")
	iv := []byte("0123456789abcdef")
	key := []byte("0123456789abcdef0123456789abcdef")

	encrypted, err := AESEncrypt(plaintext, iv, key)
	if err != nil {
		t.Errorf("AESEncrypt returned an error: %v", err)
	}

	// Test decryption to ensure correctness of encryption
	decrypted, err := AESDecrypt(encrypted, iv, key)
	if err != nil {
		t.Errorf("AESDecrypt returned an error: %v", err)
	}

	if !reflect.DeepEqual(decrypted, plaintext) {
		t.Errorf("Decrypted content does not match original plaintext")
	}
}
