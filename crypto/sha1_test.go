package crypto

import (
	"testing"
)

func TestSHA1(t *testing.T) {
	// Test case 1: empty data
	expectedHash := "da39a3ee5e6b4b0d3255bfef95601890afd80709" // SHA1 hash of an empty string
	data := []byte{}
	if hash := SHA1(data); hash != expectedHash {
		t.Errorf("SHA1(%v) = %v; want %v", data, hash, expectedHash)
	}

	// Test case 2: non-empty data
	expectedHash = "aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d" // SHA1 hash of "hello"
	data = []byte("hello")
	if hash := SHA1(data); hash != expectedHash {
		t.Errorf("SHA1(%v) = %v; want %v", data, hash, expectedHash)
	}

	// Test case 3: data with special characters
	expectedHash = "8b63fee44f3987850d8d0d6df68f4f36866ada19" // SHA1 hash of "abc123!@#$%^&*()"
	data = []byte("abc123!@#$%^&*()")
	if hash := SHA1(data); hash != expectedHash {
		t.Errorf("SHA1(%v) = %v; want %v", data, hash, expectedHash)
	}
}
