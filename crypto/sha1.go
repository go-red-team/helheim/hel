package crypto

import (
	"crypto/sha1"
	"encoding/hex"
)

// SHA1 is a cryptographic hash function that produces a 160-bit hash value known as a message digest.
func SHA1(data []byte) string {
	// Create a new SHA1 hasher
	hasher := sha1.New()

	// Write the data to the hasher
	hasher.Write(data)

	// Calculate the SHA1 hash
	hash := hasher.Sum(nil)

	// Return the hash as a hexadecimal string
	return hex.EncodeToString(hash)
}

// VerifySHA1 verifies the SHA1 hash of the given data and returns true if the hash is valid, and false otherwise.
func VerifySHA1(data []byte, hash string) bool {
	// Compare the calculated hash with the provided hash
	return SHA1(data) == hash
}
