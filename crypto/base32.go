package crypto

import (
	"encoding/base32"
	"errors"
)

// Base32Encode encodes the given data using base32 encoding.
func Base32Encode(data string) string {
	// Encode the data using base32 encoding and return the result.
	return base32.StdEncoding.EncodeToString([]byte(data))

}

// Base32Decode decodes the given base32 encoded data.
func Base32Decode(data string) (string, error) {
	// Decode the data using base32 encoding and return the result.
	decoded, err := base32.StdEncoding.DecodeString(data)

	// If an error occurred, return it.
	if err != nil {
		return "", errors.New("failed to decode base32 data")
	}

	// Return the decoded data as a string.
	return string(decoded), nil
}
