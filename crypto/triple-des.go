package crypto

import (
	"bytes"
	"crypto/cipher"
	"crypto/des"
	"errors"
)

// TripleDESEncrypt encrypts the given data using Triple DES
func TripleDESEncrypt(data, key []byte) ([]byte, error) {
	// Create a new Triple DES cipher
	block, err := des.NewTripleDESCipher(key)
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// Apply PKCS7 Padding
	padding := block.BlockSize() - len(data)%block.BlockSize()
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	data = append(data, padtext...)

	// Create a slice of bytes to store the ciphertext
	ciphertext := make([]byte, len(data))

	// Encrypt the data using the Triple DES cipher
	cipher.NewCBCEncrypter(block, key[:block.BlockSize()]).CryptBlocks(ciphertext, data)

	// Return the ciphertext
	return ciphertext, nil
}

// TripleDESDecrypt decrypts the given data using Triple DES
func TripleDESDecrypt(data, key []byte) ([]byte, error) {
	// Create a new Triple DES cipher
	block, err := des.NewTripleDESCipher(key)

	// Check if the cipher was created successfully
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// Create a slice of bytes to store the plaintext
	plaintext := make([]byte, len(data))

	// Decrypt the data using the Triple DES cipher
	cipher.NewCBCDecrypter(block, key[:block.BlockSize()]).CryptBlocks(plaintext, data)

	// Return the plaintext
	return plaintext, nil
}
