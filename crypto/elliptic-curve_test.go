package crypto

import (
	"testing"
)

func TestEllipticCurveEncrypt(t *testing.T) {
	data := []byte("Hello, World!")
	key := []byte("0123456789abcdef")

	ciphertext, err := EllipticCurveEncrypt(data, key)

	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	// Decrypt the ciphertext using the same key
	decryptedData, err := EllipticCurveDecrypt(ciphertext, key)

	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	// Verify that the decrypted data matches the original data
	if string(decryptedData) != string(data) {
		t.Errorf("Decrypted data does not match original data")
	}
}
