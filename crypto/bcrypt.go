package crypto

import (
	"errors"

	"golang.org/x/crypto/bcrypt"
)

// Hash hashes the given data and returns a byte slice containing the hash.
func BcryptHash(plaintext []byte) ([]byte, error) {
	// Hash the plaintext using bcrypt
	hash, err := bcrypt.GenerateFromPassword(plaintext, bcrypt.DefaultCost)

	// If an error occurred, return it.
	if err != nil {
		return nil, errors.New("failed to hash plaintext")
	}

	// Return the hash.
	return hash, nil
}

// BcryptVerifyHash verifies that the given hash matches the given data.
func BcryptVerifyHash(hash, plaintext []byte) bool {
	// Compare the hash to the plaintext using bcrypt.
	verify := bcrypt.CompareHashAndPassword(hash, plaintext)

	// If the hash and plaintext match, return true.
	return verify == nil
}
