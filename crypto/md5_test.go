package crypto

import (
	"testing"
)

func TestMD5Hash(t *testing.T) {
	// Test case 1: Test with empty data
	data := []byte{}
	expectedHash := "d41d8cd98f00b204e9800998ecf8427e"
	if result := MD5Hash(data); result != expectedHash {
		t.Errorf("MD5Hash([]byte{}) = %v; want %v", result, expectedHash)
	}

	// Test case 2: Test with non-empty data
	data = []byte("Hello, World!")
	expectedHash = "65a8e27d8879283831b664bd8b7f0ad4"
	if result := MD5Hash(data); result != expectedHash {
		t.Errorf("MD5Hash([]byte(\"Hello, World!\")) = %v; want %v", result, expectedHash)
	}
}
