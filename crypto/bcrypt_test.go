package crypto

import (
	"testing"

	"golang.org/x/crypto/bcrypt"
)

func TestBcryptHash(t *testing.T) {
	plaintext := []byte("password123")

	hash, err := BcryptHash(plaintext)
	if err != nil {
		t.Errorf("BcryptHash returned an error: %v", err)
	}

	err = bcrypt.CompareHashAndPassword(hash, plaintext)
	if err != nil {
		t.Errorf("bcrypt.CompareHashAndPassword returned an error: %v", err)
	}
}
