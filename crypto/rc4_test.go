package crypto

import (
	"reflect"
	"testing"
)

func TestRC4Encrypt(t *testing.T) {
	// Test case 1: Encrypting empty data should return an empty ciphertext
	key := []byte("mykey")
	data := []byte{}
	expected := []byte{}
	ciphertext, err := RC4Encrypt(data, key)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if !reflect.DeepEqual(ciphertext, expected) {
		t.Errorf("RC4Encrypt() = %v; want %v", ciphertext, expected)
	}

	// Test case 2: Encrypting non-empty data should return a non-empty ciphertext
	data = []byte("hello")
	ciphertext, err = RC4Encrypt(data, key)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	// Decrypt the ciphertext
	decrypted, err := RC4Decrypt(ciphertext, key)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	// Check that the decrypted data matches the original data
	if !reflect.DeepEqual(decrypted, data) {
		t.Errorf("RC4Decrypt(RC4Encrypt(%v)) = %v; want %v", data, decrypted, data)
	}
}
