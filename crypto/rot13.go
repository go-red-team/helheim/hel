package crypto

import "strings"

// ROT13 encodes the given data using the ROT13 cipher and returns the ciphertext.
func ROT13(data string) string {
	// Create a variable to store the result
	var result strings.Builder

	// Iterate over each character in the input string
	for _, r := range data {
		switch {
		case 'a' <= r && r <= 'z': // If the character is a lowercase letter
			result.WriteRune('a' + (r-'a'+13)%26) // Rotate the character by 13 positions
		case 'A' <= r && r <= 'Z': // If the character is an uppercase letter
			result.WriteRune('A' + (r-'A'+13)%26) // Rotate the character by 13 positions
		default:
			result.WriteRune(r) // If the character is not a letter, keep it as it is
		}
	}

	// Return the result as a string
	return result.String()
}
