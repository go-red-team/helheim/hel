package crypto

import (
	"testing"
)

func TestTripleDESEncrypt(t *testing.T) {
	// Test case 1: Encrypting empty data should return an empty ciphertext
	emptyData := []byte{}
	emptyKey := make([]byte, 24)
	emptyCiphertext, err := TripleDESEncrypt(emptyData, emptyKey)
	if err != nil {
		t.Errorf("TripleDESEncrypt(emptyData, emptyKey) returned an error: %v", err)
	}
	if len(emptyCiphertext) != 8 {
		t.Errorf("TripleDESEncrypt(emptyData, emptyKey) = %v; want empty ciphertext", emptyCiphertext)
	}

	// Test case 2: Encrypting non-empty data should return a non-empty ciphertext
	data := []byte("Hello, World!")
	key := []byte("0123456789ABCDEFGHJKLMNO")
	ciphertext, err := TripleDESEncrypt(data, key)
	if err != nil {
		t.Errorf("TripleDESEncrypt(data, key) returned an error: %v", err)
	}
	if len(ciphertext) == 0 {
		t.Errorf("TripleDESEncrypt(data, key) returned an empty ciphertext")
	}
}
