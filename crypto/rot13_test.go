package crypto

import (
	"testing"
)

func TestROT13(t *testing.T) {
	// Test case 1: ROT13 of "hello" should be "uryyb"
	if result := ROT13("hello"); result != "uryyb" {
		t.Errorf("ROT13(\"hello\") = %s; want \"uryyb\"", result)
	}

	// Test case 2: ROT13 of "HELLO" should be "URYYB"
	if result := ROT13("HELLO"); result != "URYYB" {
		t.Errorf("ROT13(\"HELLO\") = %s; want \"URYYB\"", result)
	}

	// Test case 3: ROT13 of "12345" should be "12345" (no change)
	if result := ROT13("12345"); result != "12345" {
		t.Errorf("ROT13(\"12345\") = %s; want \"12345\"", result)
	}

	// Test case 4: ROT13 of "abcXYZ" should be "nopKLM"
	if result := ROT13("abcXYZ"); result != "nopKLM" {
		t.Errorf("ROT13(\"abcXYZ\") = %s; want \"nopKLM\"", result)
	}

	// Test case 5: ROT13 of "Go Red Team!" should be "Tb Erq Grnz!"
	if result := ROT13("Go Red Team!"); result != "Tb Erq Grnz!" {
		t.Errorf("ROT13(\"Go Red Team!\") = %s; want \"Tb Erq Grnz!\"", result)
	}
}
