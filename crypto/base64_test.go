package crypto

import (
	"testing"
)

func TestBase64Decode(t *testing.T) {
	// Test case 1: valid base64 encoded string
	input1 := "SGVsbG8gd29ybGQh"
	expected1 := "Hello world!"
	result1, err1 := Base64Decode(input1)
	if err1 != nil {
		t.Errorf("Base64Decode(%q) returned an error: %v", input1, err1)
	}
	if result1 != expected1 {
		t.Errorf("Base64Decode(%q) = %q; want %q", input1, result1, expected1)
	}

	// Test case 2: empty string
	input2 := ""
	expected2 := ""
	result2, err2 := Base64Decode(input2)
	if err2 != nil {
		t.Errorf("Base64Decode(%q) returned an error: %v", input2, err2)
	}
	if result2 != expected2 {
		t.Errorf("Base64Decode(%q) = %q; want %q", input2, result2, expected2)
	}

	// Test case 3: invalid base64 encoded string
	input3 := "SGVsbG8gd29ybGQh!"
	expected3 := ""
	result3, err3 := Base64Decode(input3)
	if err3 == nil {
		t.Errorf("Base64Decode(%q) did not return an error", input3)
	}
	if result3 != expected3 {
		t.Errorf("Base64Decode(%q) = %q; want %q", input3, result3, expected3)
	}
}
