package crypto

import (
	"testing"
)

func TestBase32Decode(t *testing.T) {
	// Test case 1: Decoding a valid base32 string
	input1 := "MZXW6YTBOI======"
	expected1 := "foobar"
	result1, err1 := Base32Decode(input1)
	if err1 != nil {
		t.Errorf("Base32Decode(%q) returned an error: %v", input1, err1)
	}
	if result1 != expected1 {
		t.Errorf("Base32Decode(%q) = %q; want %q", input1, result1, expected1)
	}

	// Test case 2: Decoding an invalid base32 string
	input2 := "12345"
	expected2 := ""
	result2, err2 := Base32Decode(input2)
	if err2 == nil {
		t.Errorf("Base32Decode(%q) did not return an error", input2)
	}
	if result2 != expected2 {
		t.Errorf("Base32Decode(%q) = %q; want %q", input2, result2, expected2)
	}
}
