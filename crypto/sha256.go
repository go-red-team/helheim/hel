package crypto

import (
	"crypto/sha256"
	"encoding/hex"
)

// SHA256 is a cryptographic hash function that produces a 256-bit hash value known as a message digest.
func SHA256(data []byte) string {
	// Create a new hasher
	hasher := sha256.New()

	// Write the data to the hasher
	hasher.Write(data)

	// Calculate the SHA256 hash
	hash := hasher.Sum(nil)

	// Return the hash as a hexadecimal string
	return hex.EncodeToString(hash)
}

// VerifySHA256 verifies the SHA256 hash of the given data and returns true if the hash is valid, and false otherwise.
func VerifySHA256(data []byte, hash string) bool {
	// Compare the calculated hash with the provided hash
	return SHA256(data) == hash
}
