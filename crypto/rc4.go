package crypto

import (
	"crypto/rc4"
	"errors"
)

// RC4Encypt encrypts the given data using the given key and returns the ciphertext.
func RC4Encrypt(data, key []byte) ([]byte, error) {
	// Create a new RC4 cipher using the given key
	cipher, err := rc4.NewCipher(key)

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// Make a slice of bytes the length of the plaintext
	ciphertext := make([]byte, len(data))

	// Encrypt the data
	cipher.XORKeyStream(ciphertext, data)

	// Return the ciphertext
	return ciphertext, nil
}

// RC4Decrypt decrypts the given ciphertext using the given key and returns the plaintext.
func RC4Decrypt(data, key []byte) ([]byte, error) {
	// Create a new RC4 cipher using the given key
	cipher, err := rc4.NewCipher(key)

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// Make a slice of bytes the length of the ciphertext
	plaintext := make([]byte, len(data))

	// Decrypt the data
	cipher.XORKeyStream(plaintext, data)

	// Return the plaintext
	return plaintext, nil
}
