package crypto

import (
	"encoding/base64"
	"errors"
)

// Base64Encode encodes the given data using base64 encoding.
func Base64Encode(data string) string {
	// Encode the data using base64 encoding and return the result.
	return base64.StdEncoding.EncodeToString([]byte(data))
}

// Base64Decode decodes the given data using base64 encoding.
func Base64Decode(data string) (string, error) {
	// Decode the data using base64 encoding and return the result.
	decoded, err := base64.StdEncoding.DecodeString(data)

	// If an error occurred, return it.
	if err != nil {
		return "", errors.New("failed to decode base64 data")
	}

	// Return the decoded data as a string.
	return string(decoded), nil
}
