package crypto

import (
	"reflect"
	"testing"
)

func TestPKCS5Pad(t *testing.T) {
	expectedOutput1 := []byte{72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33, 3, 3, 3}
	expectedOutput2 := []byte{72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33, 3, 3, 3}

	input := []byte("Hello, World!")

	// Test with block size 8
	result := PKCS5Pad(input, 8)
	if !reflect.DeepEqual(result, expectedOutput1) {
		t.Errorf("PKCS5Pad(%v, 8) = %v; want %v", input, result, expectedOutput1)
	}

	// Test with block size 16
	result = PKCS5Pad(input, 16)
	if !reflect.DeepEqual(result, expectedOutput2) {
		t.Errorf("PKCS5Pad(%v, 16) = %v; want %v", input, result, expectedOutput2)
	}
}
