package crypto

import "strings"

// ROT47 encodes the given data using the ROT47 cipher and returns the ciphertext.
func ROT47(data string) string {
	// Create a variable to store the result
	var result strings.Builder

	// Iterate over each character in the input string
	for _, r := range data {
		if r >= '!' && r <= '~' { // If the character is a printable ASCII character
			result.WriteRune(rune((r-'!'+47)%94 + '!')) // Rotate the character by 47 positions
		} else {
			result.WriteRune(r) // If the character is not a printable ASCII character, keep it as it is
		}
	}

	// Return the result
	return result.String()
}
