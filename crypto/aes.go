package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
)

// AESEncrypt encrypts the given plaintext using the given key and returns the ciphertext.
func AESEncrypt(plaintext, iv, key []byte) ([]byte, error) {
	// Create a new key block using the given key.
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// If the plaintext is empty, return an error.
	if string(plaintext) == "" {
		return []byte(""), errors.New("plaintext is empty")
	}
	// Create a new CBC Encrypter using the key block and IV.
	cbcEncrypter := cipher.NewCBCEncrypter(block, iv)

	// Convert the plaintext to a byte slice.
	content := []byte(plaintext)

	// Add padding to the content.
	content = PKCS5Pad(content, block.BlockSize())

	// Create a byte slice to store the encrypted content.
	crypt := make([]byte, len(content))

	// Encrypt the content.
	cbcEncrypter.CryptBlocks(crypt, content)

	// Return the encrypted content.
	return crypt, nil
}

// AESDecrypt decrypts the given ciphertext using the given key and returns the plaintext.
func AESDecrypt(ciphertext, iv, key []byte) ([]byte, error) {
	// Create a new key block using the given key.
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// If the ciphertext is empty, return an error.
	if len(ciphertext) == 0 {
		return []byte(""), errors.New("ciphertext is empty")
	}
	// Create a new CBC Decrypter using the key block and IV.
	cbcDecrypter := cipher.NewCBCDecrypter(block, iv)

	// Create a byte slice to store the plaintext.
	plaintext := make([]byte, len(ciphertext))

	// Decrypt the ciphertext.
	cbcDecrypter.CryptBlocks(plaintext, ciphertext)

	// Return the plaintext without the padding.
	return PKCS5Trim(plaintext), nil
}

// AESGenerateIV generates a random initialization vector (IV) of the given length.
func AESGenerateIV() ([]byte, error) {
	iv := make([]byte, aes.BlockSize) // Create a byte slice to store the IV.
	_, err := rand.Read(iv)           // Generate a random IV.

	// If an error occurred, return it.
	if err != nil {
		return nil, errors.New("failed to generate IV")
	}

	// Return the generated IV.
	return iv, nil
}
