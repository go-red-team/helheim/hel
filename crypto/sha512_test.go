package crypto

import (
	"testing"
)

func TestSHA512(t *testing.T) {
	// Test case 1: Test with empty data
	data := []byte{}
	expectedHash := "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"
	if result := SHA512(data); result != expectedHash {
		t.Errorf("SHA512([]byte{}) = %v; want %v", result, expectedHash)
	}

	// Test case 2: Test with non-empty data
	data = []byte("Hello, World!")
	expectedHash = "374d794a95cdcfd8b35993185fef9ba368f160d8daf432d08ba9f1ed1e5abe6cc69291e0fa2fe0006a52570ef18c19def4e617c33ce52ef0a6e5fbe318cb0387"
	if result := SHA512(data); result != expectedHash {
		t.Errorf("SHA512([]byte(\"Hello, World!\")) = %v; want %v", result, expectedHash)
	}
}
