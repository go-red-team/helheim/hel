package crypto

import (
	"testing"
)

func TestROT47(t *testing.T) {
	// Test case 1: ROT47 of an empty string should return an empty string
	if result := ROT47(""); result != "" {
		t.Errorf("ROT47(\"\") = %s; want \"\"", result)
	}

	// Test case 2: ROT47 of a string with printable ASCII characters
	if result := ROT47("Hello, World!"); result != "w6==@[ (@C=5P" {
		t.Errorf("ROT47(\"Hello, World!\") = %s; want \"w6==@[ (@C=5P\"", result)
	}

	// Test case 3: ROT47 of a string with non-printable ASCII characters
	if result := ROT47("123@#$"); result != "`aboRS" {
		t.Errorf("ROT47(\"123@#$\") = %s; want \"`aboRS\"", result)
	}
}
