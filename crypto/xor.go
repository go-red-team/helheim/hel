package crypto

// XOR applies the XOR operation to the given data.
func XOR(buf, xor []byte) []byte {
	// Create a slice of bytes to store the result
	result := make([]byte, len(buf))

	// Apply the XOR operation to each byte in the data
	for i := 0; i < len(buf); i++ {
		result[i] = xor[i%len(xor)] ^ buf[i] // XOR the byte with the corresponding byte in the XOR key
	}

	// Return the result
	return result
}
