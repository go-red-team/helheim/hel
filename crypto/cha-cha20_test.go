package crypto

import (
	"testing"
)

func TestChaCha20Encrypt(t *testing.T) {
	// Test case 1: Encrypting empty data should return a ciphertext with length equal to the authentication tag
	data := []byte{}
	key := make([]byte, 32)
	ciphertext, err := ChaCha20Encrypt(data, key)
	if err != nil {
		t.Errorf("ChaCha20Encrypt failed with error: %v", err)
	}
	if len(ciphertext) != 16 {
		t.Errorf("ChaCha20Encrypt returned ciphertext with length 16; want %d", len(ciphertext))
	}

	// Test case 2: Encrypting non-empty data should return a non-empty ciphertext
	data = []byte("Hello, World!")
	ciphertext, err = ChaCha20Encrypt(data, key)
	if err != nil {
		t.Errorf("ChaCha20Encrypt failed with error: %v", err)
	}
	if len(ciphertext) <= len(data) {
		t.Errorf("ChaCha20Encrypt returned a ciphertext with length less than or equal to the plaintext")
	}

	// Test case 3: Encrypting data with an invalid key length should return an error
	invalidKey := make([]byte, 16)
	_, err = ChaCha20Encrypt(data, invalidKey)
	if err == nil {
		t.Errorf("ChaCha20Encrypt did not return an error for invalid key length")
	}
}
