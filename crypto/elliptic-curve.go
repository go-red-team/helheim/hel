package crypto

import (
	"encoding/hex"
	"errors"

	ecies "github.com/ecies/go/v2"
)

// EllipticCurveEncrypt encrypts the given data using the given key and returns the ciphertext.
func EllipticCurveEncrypt(data, key []byte) ([]byte, error) {
	hexKey := hex.EncodeToString(key)            // Encode the key as a hex string
	k, err := ecies.NewPrivateKeyFromHex(hexKey) // Create a new private key from the hex string

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to create new private key")
	}

	ciphertext, err := ecies.Encrypt(k.PublicKey, data) // Encrypt the data using the public key

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to encrypt data")
	}

	// Return ciphertext
	return ciphertext, nil
}

// EllipticCurveDecrypt decrypts the given ciphertext using the given key and returns the plaintext.
func EllipticCurveDecrypt(data, key []byte) ([]byte, error) {
	hexKey := hex.EncodeToString(key)            // Encode the key as a hex string
	k, err := ecies.NewPrivateKeyFromHex(hexKey) // Create a new private key from the hex string

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to create new private key")
	}

	decrypt, err := ecies.Decrypt(k, data) // Decrypt the data using the private key

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to decrypt data")
	}

	// Return the decrypted data
	return decrypt, nil
}
