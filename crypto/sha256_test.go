package crypto

import (
	"testing"
)

func TestSHA256(t *testing.T) {
	// Test case 1: Test with empty data
	data := []byte{}
	expectedHash := "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
	if result := SHA256(data); result != expectedHash {
		t.Errorf("SHA256([]byte{}) = %v; want %v", result, expectedHash)
	}

	// Test case 2: Test with non-empty data
	data = []byte("Hello, World!")
	expectedHash = "dffd6021bb2bd5b0af676290809ec3a53191dd81c7f70a4b28688a362182986f"
	if result := SHA256(data); result != expectedHash {
		t.Errorf("SHA256([]byte(\"Hello, World!\")) = %v; want %v", result, expectedHash)
	}
}
