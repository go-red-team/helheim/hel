package crypto

import "bytes"

// PKCS5Trim removes the padding from the given content.
func PKCS5Trim(content []byte) []byte {
	padding := content[len(content)-1]         // Get the last byte of the content.
	return content[:len(content)-int(padding)] // Return the content without the padding.
}

// PKCS5Pad adds padding to the given content.
func PKCS5Pad(content []byte, blockSize int) []byte {
	padding := blockSize - len(content)%blockSize           // Calculate the padding.
	padText := bytes.Repeat([]byte{byte(padding)}, padding) // Create a slice of padding bytes.
	return append(content, padText...)                      // Append the padding to the content.
}
