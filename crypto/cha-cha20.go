package crypto

import (
	"crypto/rand"
	"errors"

	"golang.org/x/crypto/chacha20poly1305"
)

// ChaCha20Encrypt encrypts the given plaintext using the given key and returns the ciphertext.
func ChaCha20Encrypt(data, key []byte) ([]byte, error) {
	// If the key is not 32 bytes, return an error
	if len(key) != 32 {
		return nil, errors.New("key must be 32 bytes")
	}

	// Create a new ChaCha20-Poly1305 cipher using the key
	c, err := chacha20poly1305.New(key)
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// Generate a random nonce
	nonceSize := c.NonceSize()
	nonce := make([]byte, nonceSize, nonceSize+len(data)+c.Overhead())

	// Read random bytes into the nonce
	_, err = rand.Read(nonce)

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to generate nonce")
	}

	// Encrypt the data using the cipher
	ciphertext := c.Seal(nil, nonce, data, nil)

	// Return ciphertext
	return ciphertext, nil
}

// ChaCha20Decrypt decrypts the given ciphertext using the given key and returns the plaintext.
func ChaCha20Decrypt(data, key []byte) ([]byte, error) {

	// If the key is not 32 bytes, return an error
	if len(key) != 32 {
		return nil, errors.New("key must be 32 bytes")
	}

	// Create a new ChaCha20-Poly1305 cipher using the key
	c, err := chacha20poly1305.New(key)

	// If error occurred, return it
	if err != nil {
		return nil, errors.New("failed to create new cipher block")
	}

	// Split the nonce and ciphertext
	nonce, ciphertext := data[:c.NonceSize()], data[c.NonceSize():]

	// Decrypt the data using the cipher
	plaintext, err := c.Open(nil, nonce, ciphertext, nil)

	// If an error occurred, return it
	if err != nil {
		return nil, errors.New("failed to decrypt data")
	}

	// Return the plaintext
	return plaintext, nil

}
