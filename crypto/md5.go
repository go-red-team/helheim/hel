package crypto

import (
	"crypto/md5"
	"encoding/hex"
)

// MD5Hash returns the MD5 hash of the given data.
func MD5Hash(data []byte) string {
	// Generate an md5 hash of the data
	md5hash := md5.Sum(data)

	// Return the md5 hash as a string
	return hex.EncodeToString(md5hash[:])
}

// VerifyMD5Hash verifies that the given data matches the given MD5 hash.
func VerifyMD5Hash(hash, password string) bool {
	passHash := MD5Hash([]byte(password))

	// Compare the given hash with the hash of the password
	return passHash == hash
}
