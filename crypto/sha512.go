package crypto

import (
	"crypto/sha512"
	"encoding/hex"
)

// SHA512 is a cryptographic hash function that produces a 512-bit hash value known as a message digest.
func SHA512(data []byte) string {
	// Create a new SHA512 hasher
	hasher := sha512.New()

	// Write the data to the hasher
	hasher.Write(data)

	// Calculate the SHA512 hash
	hash := hasher.Sum(nil)

	// Return the hash as a hexadecimal string
	return hex.EncodeToString(hash)
}

// VerifySHA512 verifies the SHA512 hash of the given data and returns true if the hash is valid, and false otherwise.
func VerifySHA512(data []byte, hash string) bool {
	// Compare the calculated hash with the provided hash
	return SHA512(data) == hash
}
