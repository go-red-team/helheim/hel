package environment

import (
	"fmt"
	"os/user"
)

// GetUsername returns the username of the current user.
// It retrieves the current user using the user.Current() function.
// If an error occurs while getting the current user, it returns an empty string and the error.
func GetUsername() (string, error) {
	currentUser, err := user.Current()
	if err != nil {
		return "", fmt.Errorf("could not get current user: %w", err)
	}
	return currentUser.Username, nil
}
