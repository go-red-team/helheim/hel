package environment

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetOSInfo(t *testing.T) {
	// Call the GetOSInfo function
	osInfo, err := GetOSInfo()

	// Assert that no error occurred
	assert.NoError(t, err, "GetOSInfo should not return an error")

	// Assert that the OSInfo struct is not empty
	assert.NotEmpty(t, osInfo, "GetOSInfo should return a non-empty OSInfo struct")

	// Assert that the Platform, Family, Version, and KernelVersion fields are not empty
	assert.NotEmpty(t, osInfo.Platform, "Platform should not be empty")
	assert.NotEmpty(t, osInfo.Family, "Family should not be empty")
	assert.NotEmpty(t, osInfo.Version, "Version should not be empty")
	assert.NotEmpty(t, osInfo.KernelVersion, "KernelVersion should not be empty")
}
