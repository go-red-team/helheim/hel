package environment

import (
	"fmt"
	"os/user"
)

// GetGroups retrieves the names of the groups that the current user belongs to.
// It returns a slice of strings representing the group names and an error if any.
func GetGroups() ([]string, error) {
	// Get current user
	currentUser, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("could not get current user: %w", err)
	}

	// Get groups
	groupIDs, err := currentUser.GroupIds()
	if err != nil {
		return nil, fmt.Errorf("could not get group IDs: %w", err)
	}

	// Get Group names
	groups := make([]string, len(groupIDs))
	// Iterate over group IDs
	for i, groupID := range groupIDs {
		// Lookup group by ID
		group, err := user.LookupGroupId(groupID)
		if err != nil {
			return nil, fmt.Errorf("could not lookup group: %w", err)
		}
		// Add group name to list
		groups[i] = group.Name
	}

	// Return groups
	return groups, nil
}
