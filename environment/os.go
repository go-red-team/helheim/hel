package environment

import (
	"github.com/shirou/gopsutil/v3/host"
)

type OSInfo struct {
	Platform      string
	Family        string
	Version       string
	KernelVersion string
}

// GetOSInfo retrieves information about the operating system.
// It returns an OSInfo struct containing the platform, family, version, and kernel version of the operating system.
// If an error occurs during the retrieval of the information, it returns an empty OSInfo struct and the error.
func GetOSInfo() (OSInfo, error) {
	// Get the Host information
	info, err := host.Info()

	if err != nil {
		return OSInfo{}, err
	}

	// Return the OS information
	return OSInfo{
		Platform:      info.Platform,
		Family:        info.PlatformFamily,
		Version:       info.PlatformVersion,
		KernelVersion: info.KernelVersion,
	}, nil
}
