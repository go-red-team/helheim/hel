package environment

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetUsername(t *testing.T) {
	// Call the GetUsername function
	username, err := GetUsername()

	// Assert that no error occurred
	assert.NoError(t, err, "GetUsername should not return an error")

	// Assert that the username is not empty
	assert.NotEmpty(t, username, "GetUsername should return a non-empty username")
}
