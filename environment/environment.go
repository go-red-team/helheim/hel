package environment

import "os"

// GetEnvironment returns the current environment variables as a slice of strings.
func GetEnvironment() []string {
	return os.Environ()
}
