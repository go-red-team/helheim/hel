package environment

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetEnvironment(t *testing.T) {
	// Call the GetEnvironment function
	env := GetEnvironment()

	// Assert that the environment is not empty
	assert.NotEmpty(t, env, "GetEnvironment should return a non-empty slice")

	// Assert that the environment contains the "PATH" variable
	var containsPath bool
	for _, e := range env {
		if e[:4] == "PATH" {
			containsPath = true
			break
		}
	}
	assert.True(t, containsPath, "Environment should contain the PATH variable")
}
