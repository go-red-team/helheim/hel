package environment

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCurrentDomain(t *testing.T) {
	domain, err := GetCurrentDomain()

	// Assert that no error occurred
	assert.NoError(t, err, "GetCurrentDomain should not return an error")

	// Assert that the domain is not empty
	assert.NotEmpty(t, domain, "GetCurrentDomain should return a non-empty domain")
}

func TestIsDomainJoined(t *testing.T) {
	isJoined, err := IsDomainJoined()

	// Assert that no error occurred
	assert.NoError(t, err, "IsDomainJoined should not return an error")

	// Assert that isJoined is a boolean
	assert.IsType(t, true, isJoined, "IsDomainJoined should return a boolean")
}
