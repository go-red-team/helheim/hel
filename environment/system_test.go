package environment

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetSystemInfo(t *testing.T) {
	// Call the GetSystemInfo function
	systemInfo := GetSystemInfo()

	// Assert that the total memory is greater than 0
	assert.Greater(t, systemInfo.TotalMemory, int64(0), "Total memory should be greater than 0")

	// Assert that the CPU model is not empty
	assert.NotEmpty(t, systemInfo.CPUModel, "CPU model should not be empty")

	// Assert that the CPU cores is greater than 0
	assert.Greater(t, systemInfo.CPUCores, int32(0), "CPU cores should be greater than 0")
}
