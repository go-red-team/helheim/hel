package environment

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetGroups(t *testing.T) {
	// Call the GetGroups function
	groups, err := GetGroups()

	// Assert that no error occurred
	assert.NoError(t, err, "GetGroups should not return an error")

	// Assert that the groups slice is not empty
	assert.NotEmpty(t, groups, "GetGroups should return a non-empty slice")
}
