//go:build windows
// +build windows

package environment

import (
	"fmt"
	"os/user"
	"strings"
	"unsafe"

	"golang.org/x/sys/windows"
)

// GetCurrentDomain returns the domain of the current user.
// It retrieves the current user and splits the username to extract the domain.
// If the domain cannot be parsed, an error is returned.
func GetCurrentDomain() (string, error) {
	// Get the current user
	currentUser, err := user.Current()
	if err != nil {
		return "", fmt.Errorf("could not get current user: %w", err)
	}

	// Split the username into the username and domain
	parts := strings.Split(currentUser.Username, "\\")
	if len(parts) < 2 {
		return "", fmt.Errorf("could not parse domain from username: %s", currentUser.Username)
	}

	// Return the domain
	return parts[0], nil
}

// IsDomainJoined checks to see if the current user is joined to a domain
func IsDomainJoined() (bool, error) {
	var domain *uint16
	var status uint32

	err := windows.NetGetJoinInformation(nil, &domain, &status)
	if err != nil {
		return false, fmt.Errorf("could not get domain join information: %w", err)
	}
	windows.NetApiBufferFree((*byte)(unsafe.Pointer(domain)))
	return status == windows.NetSetupDomainName, nil
}
