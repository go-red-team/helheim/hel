package environment

import (
	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/mem"
)

type SystemInfo struct {
	TotalMemory int64
	CPUModel    string
	CPUCores    int32
}

// GetSystemInfo retrieves information about the system.
// It returns a SystemInfo struct containing the total memory in gigabytes,
// the CPU model name, and the number of CPU cores.
func GetSystemInfo() SystemInfo {
	// Get the total memory in gigabytes
	memory, _ := mem.VirtualMemory()
	totalMemoryGB := int64(memory.Total / (1024 * 1024 * 1024))

	// Get the CPU information
	c, _ := cpu.Info()
	cpuModel := c[0].ModelName
	cpuCores := c[0].Cores

	// Output the system information
	return SystemInfo{
		TotalMemory: totalMemoryGB,
		CPUModel:    cpuModel,
		CPUCores:    cpuCores,
	}
}
