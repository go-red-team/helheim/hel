package files

import (
	"os"
)

// DeleteFile deletes a file with the specified path
func DeleteFile(path string) error {
	// Delete the file
	err := os.Remove(path)

	// If there is an error, return it
	if err != nil {
		return err
	}

	return nil
}
