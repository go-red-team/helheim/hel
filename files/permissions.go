package files

import (
	"io/fs"
	"os"
)

// ChangePermissions changes the permissions of a given file
func ChangePermissions(path string, permissions fs.FileMode) error {
	// Change the Permissions
	err := os.Chmod(path, permissions)
	if err != nil {
		return err
	}
	return nil
}
