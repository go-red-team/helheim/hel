package files

import (
	"os"
)

// CreateFile creates a new file with the specified name
func CreateFile(path string, data []byte) error {
	// Create the file
	file, err := os.Create(path)

	// If there is an error, return it
	if err != nil {
		return err
	}

	// Defer closing the file
	defer file.Close()

	// Write the data to the file
	_, err = file.Write(data)
	if err != nil {
		return err
	}

	return nil
}
