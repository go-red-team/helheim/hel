package files

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestCreateFile(t *testing.T) {
	// Test case 1: Create file with valid data
	path := "testfile.txt"
	data := []byte("Hello, World!")
	err := CreateFile(path, data)
	if err != nil {
		t.Errorf("CreateFile(%s, %v) returned an error: %v", path, data, err)
	}

	// Check if the file exists
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		t.Errorf("CreateFile(%s, %v) failed to create the file", path, data)
	}

	// Read the file content
	fileContent, err := ioutil.ReadFile(path)
	if err != nil {
		t.Errorf("Failed to read file content: %v", err)
	}

	// Check if the file content matches the expected data
	expectedData := "Hello, World!"
	if string(fileContent) != expectedData {
		t.Errorf("CreateFile(%s, %v) wrote incorrect data to the file. Expected: %s, Got: %s", path, data, expectedData, string(fileContent))
	}

	// Remove the test file
	err = os.Remove(path)
	if err != nil {
		t.Errorf("Failed to remove test file: %v", err)
	}
}
