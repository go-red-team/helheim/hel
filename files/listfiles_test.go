package files

import (
	"os"
	"reflect"
	"testing"
)

func TestListFiles(t *testing.T) {
	// Test case 1: Non-existent directory
	directory := "nonexistent_directory"
	expectedResult := []string{}
	result, err := ListFiles(directory)
	if err == nil || !os.IsNotExist(err) {
		t.Errorf("ListFiles(%q) = %v, %v; want %v, file does not exist error", directory, result, err, expectedResult)
	}

	// Test case 2: Directory with files
	directory = "test_directory"
	err = os.Mkdir(directory, 0755)
	if err != nil {
		t.Fatalf("Failed to create test directory: %v", err)
	}
	defer os.Remove(directory)

	// Create test files in the directory
	testFiles := []string{"file1.txt", "file2.txt", "file3.txt"}
	for _, file := range testFiles {
		f, err := os.Create(directory + "/" + file)
		if err != nil {
			t.Fatalf("Failed to create test file: %v", err)
		}
		f.Close()
	}
	expectedResult = testFiles
	result, err = ListFiles(directory)
	if err != nil || !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("ListFiles(%q) = %v, %v; want %v, nil", directory, result, err, expectedResult)
	}

	// Clean up test files
	for _, file := range testFiles {
		err := os.Remove(directory + "/" + file)
		if err != nil {
			t.Errorf("Failed to remove test file: %v", err)
		}
	}
}
