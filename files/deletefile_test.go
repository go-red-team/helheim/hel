package files

import (
	"errors"
	"os"
	"testing"
)

func TestDeleteFile(t *testing.T) {
	// Test case 1: Deleting non-existent file
	err := DeleteFile("nonexistent_file.txt")
	if !errors.Is(err, os.ErrNotExist) {
		t.Errorf("DeleteFile(nonexistent_file.txt) returned unexpected error: %v", err)
	}

	// Test case 2: Deleting existing file
	// Create a test file
	file, err := os.Create("test_file.txt")
	if err != nil {
		t.Fatalf("Failed to create test file: %v", err)
	}
	file.Close()

	err = DeleteFile("test_file.txt")
	if err != nil {
		t.Errorf("DeleteFile(test_file.txt) returned unexpected error: %v", err)
	}

	// Verify that the file has been deleted
	_, err = os.Stat("test_file.txt")
	if !errors.Is(err, os.ErrNotExist) {
		t.Errorf("DeleteFile(test_file.txt) failed to delete the file")
	}
}
