package files

import (
	"io/fs"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChangePermissions(t *testing.T) {
	// Create a temporary file
	tmpfile, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatalf("Failed to create temporary file: %v", err)
	}
	defer os.Remove(tmpfile.Name()) // clean up

	// Define new permissions
	newPermissions := fs.FileMode(0600)

	// Call the ChangePermissions function
	err = ChangePermissions(tmpfile.Name(), newPermissions)
	if err != nil {
		t.Fatalf("Failed to change permissions: %v", err)
	}

	// Get the file info
	fileInfo, err := os.Stat(tmpfile.Name())
	if err != nil {
		t.Fatalf("Failed to get file info: %v", err)
	}

	// Check that the file is writable by the owner
	assert.True(t, fileInfo.Mode().Perm()&0200 != 0, "ChangePermissions should make the file writable by the owner")
}
