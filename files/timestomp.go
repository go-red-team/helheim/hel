package files

import (
	"errors"
	"os"
	"time"
)

// TimestompFile updates the access time and modification time of a file.
// It takes the file path, access time, and modification time as parameters.
// If the operation is successful, it returns nil. Otherwise, it returns an error.
func TimestompFile(path string, atime, mtime time.Time) error {
	err := os.Chtimes(path, atime, mtime)
	if err != nil {
		return errors.New("error timestomping file")
	}
	return nil
}
