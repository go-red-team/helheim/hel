package files

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestTimestompFile(t *testing.T) {
	// Create a temporary file
	tmpfile, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatalf("Failed to create temporary file: %v", err)
	}
	defer os.Remove(tmpfile.Name()) // clean up

	// Define a time in the past
	pastTime := time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)

	// Call the TimestompFile function
	err = TimestompFile(tmpfile.Name(), pastTime, pastTime)
	if err != nil {
		t.Fatalf("Failed to timestomp file: %v", err)
	}

	// Get the file info
	fileInfo, err := os.Stat(tmpfile.Name())
	if err != nil {
		t.Fatalf("Failed to get file info: %v", err)
	}

	// Convert the expected time to local time
	expectedTime := pastTime.In(fileInfo.ModTime().Location())

	// Check the access time and modification time
	assert.Equal(t, expectedTime, fileInfo.ModTime(), "TimestompFile should set the correct modification time")
}
