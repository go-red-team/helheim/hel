package files

import "os"

// ListFiles returns a list of files in a directory
func ListFiles(directory string) ([]string, error) {
	// Create a slice of strings called files
	var files []string

	// Handle empty directory
	if directory == "" {
		os.ReadDir(".")
	}

	// Get the files in the directory
	fileInfo, err := os.ReadDir(directory)
	if err != nil {
		return files, err
	}

	// Add the files to the slice
	for _, file := range fileInfo {
		files = append(files, file.Name())
	}

	// Return the files
	return files, nil
}
