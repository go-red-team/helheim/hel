//go:build linux || darwin
// +build linux darwin

package shell

import (
	"os/exec"
)

func ExecuteCommand(command string) string {
	// Create a byte slice called output
	var output []byte

	// Create a new command called shell
	shell := exec.Command("/bin/bash", "-c", command)

	// Store the output
	output, err := shell.CombinedOutput()
	if err != nil {
		return err.Error()
	}

	// Return the output
	return string(output)
}
