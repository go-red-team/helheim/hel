//go:build windows
// +build windows

package shell

import (
	"os/exec"
	"syscall"

	"golang.org/x/sys/windows"
)

// ExecuteCommand executes a shell command and returns the output
func ExecuteCommand(command string) string {
	// Create a byte slice called output
	var output []byte

	// Create a new command called shell
	shell := exec.Command("cmd", "/C", command)

	// Hide the window
	shell.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}

	// Get the output
	output, err := shell.CombinedOutput()
	if err != nil {
		return err.Error()
	}

	// Return the output
	return string(output)
}

// ExecutePowershell executes a powershell command and returns the output
func ExecutePowershell(command string) string {
	// Create a byte slice called output
	var output []byte

	// Create a new command called shell
	shell := exec.Command("powershell", command)

	// Hide the window
	shell.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}

	// Get the output
	output, err := shell.CombinedOutput()
	if err != nil {
		return err.Error()
	}

	// Return the output
	return string(output)
}

// ExecuteWithToken executes a command with a given token
func ExecuteWithToken(command string, token windows.Token) string {
	// Create a byte slice called output
	var output []byte

	// Create a new command called shell
	shell := exec.Command("powershell", command)

	// Set the token and hide the window
	shell.SysProcAttr = &syscall.SysProcAttr{HideWindow: true, Token: syscall.Token(token)}

	// Get the output
	output, err := shell.CombinedOutput()
	if err != nil {
		return err.Error()
	}

	// Return the output
	return string(output)
}
