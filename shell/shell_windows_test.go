package shell

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/sys/windows"
)

func TestExecuteCommandWindows(t *testing.T) {
	// Test case 1: Execute a command that should succeed
	output := ExecuteCommand("echo hello")
	assert.Equal(t, "hello\r\n", output, "ExecuteCommand should return the correct output")

	// Test case 2: Execute a command that should fail
	output = ExecuteCommand("nonexistentcommand")
	assert.Equal(t, "exit status 1", output, "ExecuteCommand should return 'exit status 1' when the command fails")
}

func TestExecutePowershell(t *testing.T) {
	// Test case 1: Execute a command that should succeed
	output := ExecutePowershell("Write-Output hello")
	assert.Equal(t, "hello\r\n", output, "ExecutePowershell should return the correct output")

	// Test case 2: Execute a command that should fail
	output = ExecutePowershell("nonexistentcommand")
	assert.Equal(t, "exit status 1", output, "ExecutePowershell should return 'exit status 1' when the command fails")
}

func TestExecuteWithToken(t *testing.T) {
	// Get the current process token with necessary permissions
	var token windows.Token
	err := windows.OpenProcessToken(windows.CurrentProcess(), windows.TOKEN_QUERY|windows.TOKEN_DUPLICATE|windows.TOKEN_ASSIGN_PRIMARY|windows.TOKEN_ADJUST_DEFAULT|windows.TOKEN_ADJUST_SESSIONID, &token)
	if err != nil {
		t.Fatalf("Failed to open process token: %v", err)
	}

	// Test case 1: Execute a command that should succeed
	output := ExecuteWithToken("Write-Output hello", token)
	assert.Equal(t, "hello\r\n", output, "ExecuteWithToken should return the correct output")

	// Test case 2: Execute a command that should fail
	output = ExecuteWithToken("nonexistentcommand", token)
	assert.Equal(t, "exit status 1", output, "ExecuteWithToken should return 'exit status 1' when the command fails")
}
