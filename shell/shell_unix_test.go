//go:build linux || darwin
// +build linux darwin

package shell

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExecuteCommand(t *testing.T) {
	// Test case 1: Execute a command that should succeed
	output := ExecuteCommand("echo hello")
	assert.Equal(t, "hello\n", output, "ExecuteCommand should return the correct output")

	// Test case 2: Execute a command that should fail
	output = ExecuteCommand("nonexistentcommand")
	assert.Equal(t, "nonexistentcommand: command not found\n", output, "ExecuteCommand should return an error message when the command fails")
}
